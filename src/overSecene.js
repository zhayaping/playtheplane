class OverScene{
    constructor(gameStage){
        this.game = gameStage.game;
        console.log(this.game)
        this.text = 'GAME OVER'
    }

    draw(){
        var ctx = this.game.context;
        var canvas = this.game.canvas;
        ctx.font = "30px Comic Sans MS";
        ctx.fillStyle = "red";
        ctx.textAlign = "center";
        ctx.fillText(this.text, canvas.width/2, canvas.height/2);
    }
}