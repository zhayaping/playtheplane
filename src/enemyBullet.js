class EnemyBullet extends Bullet{
    constructor(gameStage,image, x,y){
        super(gameStage,image, x,y);
        this.speed = 8;
        this.width = 10
        this.height = 30
    }

    update(){
        this.y += this.speed;

        this.judgeCollision();

        if(this.y > this.gameStage.game.canvas.offsetHeight){
            const roleList = this.gameStage.roleList;

            for(let i =0; i< roleList.length; i++){
                if(roleList[i] === this){
                    roleList.splice(i,1);
                }
            }
        }

    }

    judgeCollision(){
        const roleList = this.gameStage.roleList;
        for(let i=0; i<roleList.length; i++){
            if(roleList[i].name=='playerPlane' && this.isCollision(roleList[i]) && roleList[i].isLive && this.isLive ){
                this.isLive = false;
                roleList[i].isLive = false;

                const boom = new Boom(this.gameStage, this.x, this.y);
                this.gameStage.showRole(boom);

                this.gameStage.gameOver();

            }
        }
    }

    isCollision(enemy){
        const x = this.x + 25;
        const y = this.y + 40;
        const ex = enemy.x;
        const ey = enemy.y;
        if(x>ex && x<ex+80){
            if(y> ey+20 && y<ey+70){
                return true;
            }
        }
        return false;
    }
}