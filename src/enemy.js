class Enemy {
    constructor(gameStage, image, width, height) {
        this.name = 'enemy';
        this.gameStage = gameStage;
        this.game = this.gameStage.game;
        this.image = image;
        this.width = width || 60;
        this.height = height || 60;

        this.setup();

        this.fireTime = 30;
        this.coldTime = 0;

    }

    setup() {
        this.isLive = true;
        this.x = this.randomNum(0, this.game.canvas.offsetWidth - this.width);
        this.y = this.randomNum(-80, -120);
        this.speed = this.randomNum(2, 4);
    }

    fire() {
        if (!this.isLive) return;
        if (this.coldTime == 0 && Math.random() > .6) {
            const bullet = new EnemyBullet(this.gameStage, this.gameStage.game.allImages.enemyBullet, this.x + 15, this.y + 50);
            this.gameStage.showRole(bullet);
        }

        this.coldTime--;
        this.coldTime < 0 && (this.coldTime = this.fireTime);


    }

    randomNum(s, e) {
        return (s + Math.random() * e);
    }

    update() {
        this.judgeCollision();
        this.y += this.speed;
        this.fire();
        if (this.y > 700) {
            this.setup();
        }
    }

    judgeCollision() {
        const roleList = this.gameStage.roleList;
        for (let i = 0; i < roleList.length; i++) {
            if (roleList[i].name == 'playerPlane' && this.isCollision(roleList[i]) && roleList[i].isLive && this.isLive) {
                this.isLive = false;
                roleList[i].isLive = false;

                const boom = new Boom(this.gameStage, this.x, this.y);
                this.gameStage.showRole(boom);

                this.gameStage.gameOver();

            }
        }
    }

    isCollision(enemy) {
        const x = this.x;
        const y = this.y;
        const ex = enemy.x;
        const ey = enemy.y;

        if (x > ex && x < ex + 80) {
            if (y > ey + 20 && y < ey + 70) {
                return true;
            }
        }

        if (ex > x && ex < x + 60) {
            if (ey < y + 80 && ey > y) {
                return true;
            }
        }

        return false;
    }


    draw() {
        if (this.isLive) {
            const ctx = this.gameStage.game.context;
            var img = this.image;

            ctx.drawImage(img, this.x, this.y, this.width, this.height);
        }
    }
}