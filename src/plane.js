class Plane{
    constructor(gameStage, image, x, y){
        this.gameStage = gameStage;
        this.x = x;
        this.y = y;
        this.speed = 5;
        this.image = image;

        this.name = 'playerPlane'

        this.fireTime = 10;
        this.coldTime = 0;

        this.doubleFireTime = 10;
        this.doubleColdTime = 0;

        this.width = 80;
        this.height = 80;

        this.isLive= true;

        this.setup();
    }

    setup(){
        this.gameStage.registerAction('a', this.moveLeft.bind(this));
        this.gameStage.registerAction('d', this.moveRight.bind(this));
        this.gameStage.registerAction('w', this.moveTop.bind(this));
        this.gameStage.registerAction('s', this.moveBottom.bind(this));
        this.gameStage.registerAction('j', this.fire.bind(this));
        this.gameStage.registerAction('k', this.doubleFire.bind(this));
    }

    moveLeft(){
        if(this.x<=0) return;
        this.x = this.x - this.speed;
    }

    moveRight(){
        if(this.x+ this.width>= this.gameStage.game.canvas.clientWidth) return;
        this.x = this.x + this.speed;
    }

    moveTop(){
        if(this.y<=0) return;
        this.y -= this.speed;
    }

    moveBottom(){
        if(this.y+ this.height>= this.gameStage.game.canvas.clientHeight) return;
        this.y += this.speed;
    }

    fire(){
        if(!this.isLive) return;
        if(this.coldTime == 0){
            const bullet = new Bullet(this.gameStage, this.gameStage.game.allImages.bullet, this.x+15, this.y-50);
            this.gameStage.showRole(bullet);
        }

        this.coldTime -- ;

        this.coldTime < 0 && (this.coldTime = this.fireTime);


    }

    doubleFire(){
        if(!this.isLive) return;
        if(this.doubleColdTime == 0){
            const bullet1 = new Bullet(this.gameStage, this.gameStage.game.allImages.bullet, this.x-12, this.y+1);
            const bullet2 = new Bullet(this.gameStage, this.gameStage.game.allImages.bullet, this.x+42, this.y+1);
    
            this.gameStage.showRole(bullet1);
            this.gameStage.showRole(bullet2);
        }

        this.doubleColdTime -- ;

        this.doubleColdTime < 0 && (this.doubleColdTime = this.doubleFireTime);

    }

    draw(){
        if(!this.isLive) return;
        const ctx = this.gameStage.game.context;
        var img = this.image;
       
        ctx.drawImage(img,this.x,this.y, this.width, this.height);
    }
}