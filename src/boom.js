class Boom{
    constructor(gameStage, x, y){
        this.name = 'boom';
        this.x = x - 15;
        this.y = y;
        this.game = gameStage.game;
        this.image = this.game.allImages.boom;
        this.isLive = true;
        this.setup();
    }

    setup(){
        setTimeout(() => {
            this.isLive = false;
        }, 300);
    }

    draw(){
        if(this.isLive){
            const ctx = this.game.context;
            var img = this.image;
        
            ctx.drawImage(img,this.x,this.y, 80, 80);
        }
    }
}