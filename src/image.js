
class LoadImages{
    constructor(srcList=[]){
        this.srcList = srcList;
        this.loadedImages = [];

        this.loadImages(this.srcList);
    }

    getImages(){
        return  this.imagesObj;
    }

  
    requestImage(srcList){
        return new Promise((resolve)=>{
            const imgsRequest = [];

            this.srcList.map((item)=>{
                imgsRequest.push(
                    new Promise((resolve, reject)=>{
                        const img = new Image();

                        img.onload = ()=>{
                            resolve({
                                role: item.role,
                                img
                            });
                            
                        }
                        
                        img.src = item.url;
                    })
                )
            })

            Promise.all(imgsRequest).then((images)=>{
                resolve(images)
            })
        })
    }

    static
    load(cb){
        cb && cb();
    }


    async loadImages(srcList){
       const images =  await this.requestImage(srcList);
      
       this.loadedImages= images;

       let imagesObj = {}

       this.loadedImages.map((item)=>{
           imagesObj[item.role] = item.img
       })

       this.load(imagesObj)

    }
}