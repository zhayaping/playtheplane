class BackgroundImg{
    constructor(gameStage){
        this.game = gameStage.game;
        this.img = this.game.allImages.backgroundImg;
        this.x = 0;
        this.y = 0;
        this.width = this.game.canvas.width;
        this.height = this.game.canvas.height;

        console.log(this.img)
    }
    
    draw(){
       
        var ctx = this.game.context;
        var img = this.img;

        ctx.drawImage(img,this.x,this.y, this.width, this.height);
        
    }
}