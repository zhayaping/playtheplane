(() => {
    var images = new LoadImages([//加载所有图片资源文件
        {
            role: 'plane',
            url: 'imgs/plane.png'
        },
        {
            role: 'bullet',
            url: 'imgs/bullet.png'
        },
        {
            role: 'enemyBullet',
            url: 'imgs/enemyBullet.png'
        },
        {
            role: 'enemy0',
            url: 'imgs/0.png'
        },
        {
            role: 'enemy1',
            url: 'imgs/1.png'
        },
        {
            role: 'enemy2',
            url: 'imgs/2.png'
        },
        {
            role: 'enemy3',
            url: 'imgs/3.png'
        },
        {
            role: 'boom',
            url: 'imgs/boom.jpg'
        },
        {
            role: 'backgroundImg',
            url: 'imgs/bg.jpg'
        }
    ]);

    images.load = function (imagesObj) {
        main(imagesObj)
    }

    function main(imagesObj){
        var cav = document.querySelector('#cav');

        var g = new Game({
            allImages: imagesObj,
            name: 'Play the plane',
            canvasElement: cav
        })

        var plane = new Plane(g, g.game.allImages.plane, 150, 400 );
        g.showRole(plane);

        window.g = g;
    }

})()