
class Game {
    constructor(props){
        this.config = props;

        const {
            canvasElement,
            name,
            backgroundColor,
            allImages
        } = this.config;

        this.actions = {};

        this.game = {
            core: 0,
            over: false,
            canvas: canvasElement,
            context: canvasElement.getContext('2d'),
            name: name || 'Game',
            backgroundColor: backgroundColor || '#000',
            allImages
        }

        this.canvasElement = canvasElement;

        this.fps = 60;
        
        this.setup();
    }

    setup(){
        this.roleList = [];

        var backgroundImg = new BackgroundImg(this);
        this.showRole(backgroundImg);

        this.initEnemy();

        this.initEvent();//初始化监听事件

        this.loop();
    }

    initCanvas(){
        const {
            width,
            height
        } = this.game.canvas;

        const g = this.game.context;

        g.fillStyle= this.game.backgroundColor;
        g.fillRect(0,0,width,height);
    }

    initEnemy(){
        const num = this.enemyNum || 4;
        for(let i=0; i< num; i++){
            const image = this.game.allImages[ 'enemy'+ i ];
            const enemy = new Enemy(this, image)
            this.roleList.push(enemy)
        }
    }

    gameOver(){
        setTimeout(()=>{
            this.roleList = []

            this.showRole(new OverScene(this));
        },500)
    }

    update(){
        for(let role of this.roleList){
            role.update && role.update();
        }

        this.listenAction();
    }

    showRole(role){
        this.roleList.push(role)
    }

    draw(){
        this.game.context.clearRect(0,0, this.game.canvas.offsetWidth, this.game.canvas.offsetHeight);
        this.initCanvas();
        for(let i=0 ; i< this.roleList.length; i++){
            
            this.roleList[i].draw();

            if(this.roleList[i].name=='boom' && !this.roleList[i].isLive){
                this.roleList.splice(i, 1);
            }
        }
       
    }

    loop(){
        setTimeout(()=>{
            this.update();
            this.draw();

            this.loop();
        }, 1000 / this.fps)
    }

    registerAction(key, actionFunc){
        this.actions[key] = {
            dispatch: false,
            actionFunc
        }
        
    }

    listenAction(){
        const keys = Object.keys(this.actions);

        keys.map((key)=>{
            if(this.actions[key] && this.actions[key].dispatch){
                this.actions[key].actionFunc && this.actions[key].actionFunc();
            }
        })

    }

    initEvent(){
        window.addEventListener('keydown', (event)=>{
            const key = event.key;
            this.actions[key] && (this.actions[key].dispatch = true );
        })

        window.addEventListener('keyup', (event)=>{
            const key = event.key;
            this.actions[key] && (this.actions[key].dispatch = false );
        })
    }

}