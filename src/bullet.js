class Bullet{
    constructor(gameStage,image, x,y){
        this.x = x;
        this.y = y;
        this.speed = 5;
        this.image = image;
        this.gameStage = gameStage;
        this.game = this.gameStage.game;
        this.isLive = true;

        this.width = 50;
        this.height = 70;

        this.coreDom = document.querySelector('#core');
    }

    update(){
        this.y -= this.speed;

        this.judgeCollision();

        if((this.y+70) < 0){
            const roleList = this.gameStage.roleList;

            for(let i =0; i< roleList.length; i++){
                if(roleList[i] === this){
                    roleList.splice(i,1);
                }
            }
        }
    }

    addCore(core){
        this.coreDom.innerText = core;
    }

    judgeCollision(){
        const roleList = this.gameStage.roleList;
        for(let i=0; i<roleList.length; i++){
            if(roleList[i].name=='enemy' && this.isCollision(roleList[i]) && roleList[i].isLive && this.isLive ){
                this.isLive = false;
                roleList[i].isLive = false;

                const boom = new Boom(this.gameStage, this.x, this.y);
                this.gameStage.showRole(boom);

                this.game.core+=1;
                this.addCore(this.game.core)
            }
        }
    }

    isCollision(enemy){
        const x = this.x + 25;
        const y = this.y;
        const ex = enemy.x;
        const ey = enemy.y;
        if(x>ex && x<ex+60){
            if(y> ey && y<ey+60){
                return true;
            }
        }
        return false;
    }

    draw(){
        if(this.isLive){
            var ctx = this.gameStage.game.context;
            var img = this.image;

            ctx.drawImage(img,this.x,this.y, this.width, this.height);
        }
    }
}